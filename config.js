/**
* Settings for this node
**/

var settings = {
  defaultWebfolder:    '/usr/share/icecast2/web/',
  defaultConfigfolder: '/etc/icecast2/admin/',
  defaulLogfolder:     '/var/log/icecast2/',
  serverUrl:           'preroll.crosshost.com.br',
  hostname:            'preroll.crosshost.com.br'
};

module.exports = settings;